import router from '../router'
import { createStore } from 'vuex'

export default createStore({
  state: {
    usuarios: [],
    usuario: {
      nombre: '',
      correo: '',
      username: '',
      password: '',
    },

    partidos: [],
    partido: {
      fecha: '',
      local: '',
      visitante: '',
      golesLocal: '',
      golesVisitante: '',
      usuario: 1,
    },
  },
  mutations: {
    set(state, payload) {
      state.usuarios.push(payload)
      state.partidos.push(payload)
      console.log(state.usuarios)
      console.log(state.partidos)
    },
  },
  actions: {

    async setUsuarios({ commit }, usuario) {
      try {
        const res = await fetch(`http://localhost:8990/ciclo3Backendx/serviciosMintic/usuarios`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(usuario)
        })
        const dataDB = await res.json()
        console.log(dataDB)
      } catch (error) {
        console.log(error)
      }
      commit('set', usuario)
      router.push('/')
    },

    async setPartidos({ commit }, partido) {
      try {
        const res = await fetch(`http://localhost:8990/ciclo3Backendx/serviciosMintic/partidos`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(partido)
        })
        const dataDB = await res.json()
        console.log(dataDB)
      } catch (error) {
        console.log(error)
      }
      commit('set', partido)
      router.push('/partidos_listar')
    }

  },
  modules: {
  }
})
