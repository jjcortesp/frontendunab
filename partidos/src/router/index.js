import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/registro',
    name: 'Registro',
    component: () => import(/* webpackChunkName: "about" */ '../views/login/Registro.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import(/* webpackChunkName: "about" */ '../views/campeonato/Dashboard.vue')
  },
  {
    path: '/partidos_crear',
    name: 'PartidosCrear',
    component: () => import(/* webpackChunkName: "about" */ '../views/campeonato/PartidosCrear.vue')
  },
  {
    path: '/partidos_listar',
    name: 'PartidosListar',
    component: () => import(/* webpackChunkName: "about" */ '../views/campeonato/PartidosListar.vue')
  },
  {
    path: '/partidos_listar/:id',
    name: 'PartidosEditar',
    component: () => import(/* webpackChunkName: "about" */ '../views/campeonato/PartidosEditar.vue')
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
